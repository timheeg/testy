
#ifndef testy_bits_h
#define testy_bits_h

#include <cstdint>

namespace Testy
{
class Bits
{
public:
  Bits(const int32_t bits);

  bool isOn(const int8_t bit) const;

  void setOn(const int8_t bit);
  void setOff(const int8_t bit);

private:
  int32_t _bits;
};
}

#endif

