
#include "bits.h"

using Testy::Bits;

Bits::Bits(const int32_t bits)
: _bits(bits)
{}

bool Bits::isOn(const int8_t bit) const {
  if (bit > 32) return false;

  return _bits & (1 << bit);
}

void Bits::setOn(const int8_t bit) {
  _bits |= (1 << bit);
}

void Bits::setOff(const int8_t bit) {
  _bits &= 0xff ^ (1 << bit);
}

