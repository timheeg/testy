#!/bin/bash

# Bootstrap
cmake -H. -Bbuilds/shared -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX="install/testy-0.1"

# Build
cmake --build builds/shared --config Release --target install

# Archive
cmake --build builds/shared --target archive

